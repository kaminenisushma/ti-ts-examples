Command line instructions


Git global setup

git config --global user.name "Sushma Chowdary"
git config --global user.email "kaminenisushma@gmail.com"

Create a new repository

git clone git@gitlab.com:kaminenisushma/ti-ts-examples.git
cd ti-ts-examples
touch README.md
git add README.md
git commit -m "add README"
git push -u origin master

Existing folder

cd existing_folder
git init
git remote add origin git@gitlab.com:kaminenisushma/ti-ts-examples.git
git add .
git commit -m "Initial commit"
git push -u origin master

Existing Git repository

cd existing_repo
git remote add origin git@gitlab.com:kaminenisushma/ti-ts-examples.git
git push -u origin --all
git push -u origin --tags
